import logging
from itertools import count
from typing import List

import attr
import pulp

from src.actors.attacker import DiscreteAttacker, GradientAttacker
from src.actors.defender import Defender
from src.config import RootConfig
from src.neural_networks.network import NeuralNetwork
from src.visual.plotter import FakePlotter, LearningEpochsPlotter, DebugPlotter

logger = logging.getLogger(__name__)


@attr.s
class Result:
    iterations: int = attr.ib()
    zero_sum_nash_val: int = attr.ib()
    attacker_value: int = attr.ib()
    defender_value: int = attr.ib()
    ordered_actions_p1: List = attr.ib()
    probs_p1: List = attr.ib()
    ordered_actions_p2: List = attr.ib()
    probs_p2: List = attr.ib()


def get_fp_cost(probs2, played2):
    fp_cost = 0
    for action, prob in zip(played2, probs2):
        if prob == 0:
            continue
        fp_cost += action.final_fp_cost * prob
    return fp_cost


class GameSolver:
    def __init__(self, conf: RootConfig):
        self.conf = conf.model_conf

        # Define game actors
        if conf.model_conf.attacker_conf.use_gradient_descent:
            self.attacker = GradientAttacker(conf.model_conf)
        else:
            self.attacker = DiscreteAttacker(conf.model_conf)
        self.defender = Defender(conf.model_conf)

        # Set up proper plotter for plotting results
        if conf.model_conf.features_count != 2:
            self.plotter = FakePlotter()
        else:
            discr_actions = self.attacker.create_discrete_actions()
            if conf.plot_conf.learning_epochs_plotter:
                self.plotter = LearningEpochsPlotter(conf.plot_conf, discr_actions,
                                                     conf.model_conf.defender_conf.benign_data_file_name)
            else:
                self.plotter = DebugPlotter(conf.plot_conf, discr_actions,
                                            conf.model_conf.defender_conf.benign_data_file_name)

    def double_oracle(self) -> Result:
        # Get initial actions as the first ones
        played_actions_p1 = [self.attacker.get_initial_action()]
        played_actions_p2 = [self.defender.get_initial_action()]

        for i in count():
            logger.info(f'Iteration: {i}')

            # Solve current game with linear programming
            zero_sum_nash_val, probs_p1, probs_p2 = self.solve_zero_sum_game(
                played_actions_p1, played_actions_p2)

            fp_cost = get_fp_cost(probs_p2, played_actions_p2)
            logger.info(
                f'Solved iteration. zero sum game value: {zero_sum_nash_val}'
                f'\tattacker value of original game: {zero_sum_nash_val - fp_cost}'
                f'\tdefender value of original game: {-zero_sum_nash_val}')

            # Find best responses for each player given the mixture strategies
            br_p1 = self.attacker.get_best_response(played_actions_p2, probs_p2)
            br_p2 = self.defender.get_best_response(played_actions_p1, probs_p1)

            attacker_value = zero_sum_nash_val - fp_cost
            defender_value = -zero_sum_nash_val

            # Plot progress using plotter
            self.plotter.plot_iteration(i, zero_sum_nash_val,
                                        self.attacker.value_of_last_brp + fp_cost,
                                        br_p2.final_loss,
                                        played_actions_p2, probs_p2,
                                        played_actions_p1, probs_p1,
                                        br_p1, br_p2)

            # Are those new actions good enough?
            br_p1_exists = self.attacker.does_br_exists(played_actions_p1,
                                                        br_p1, attacker_value)
            br_p2_exists = self.defender.does_br_exists(br_p2,
                                                        zero_sum_nash_val)

            # If there is no new action in best responses, algorithm ends
            if br_p1_exists and br_p2_exists:
                return Result(i, zero_sum_nash_val, attacker_value,
                              defender_value, played_actions_p1, probs_p1,
                              played_actions_p2, probs_p2)

            # Otherwise add new actions to lists and continue
            if not br_p1_exists: played_actions_p1.append(br_p1)
            if not br_p2_exists: played_actions_p2.append(br_p2)

    def solve_zero_sum_game(self, actions_p1: List[List[float]],
                            actions_p2: List[NeuralNetwork]):

        logger.debug('Going to solve current state with LP')
        logger.debug(f'Attacker\'s actions by now: {actions_p1}')
        logger.debug(f'Deffender\'s actions by now: {actions_p2}')

        # Create LP problem
        m = pulp.LpProblem("Zero sum game", pulp.LpMinimize)

        # Minimizing value "v"
        v = pulp.LpVariable("v")
        m += v

        # Player two probability vector
        probs_p_two = [pulp.LpVariable("np" + str(i), 0, 1) for i in
                       range(len(actions_p2))]
        m += pulp.lpSum(probs_p_two) == 1  # Probabilities sum to 1

        # Calc false positive cost with benign data probability distribution
        fp_cost = 0
        for nn, nn_prob in zip(actions_p2, probs_p_two):
            fp_cost += nn.final_fp_cost * nn_prob

        # Define main constraints
        constraints = []
        for a1 in actions_p1:
            suma = [fp_cost]
            j = 0
            for a2 in actions_p2:
                suma.append(probs_p_two[j] * self.conf.attacker_utility(a1, a2))
                j += 1
            constraints.append(pulp.lpSum(suma) <= v)
        for c in constraints:
            m += c

        # Let's solve
        m.solve()

        logger.debug(f'LP solved')
        logger.debug(f'Value of the game: {v.varValue}')
        logger.debug(f'Number of false positives in this game: {fp_cost}')
        logger.debug(f'Found solution: {pulp.LpStatus[m.status]}')
        logger.debug(f'Attacker\' probabilities:')
        logger.debug(f'{list(str(abs(c.pi)) + " " for c in constraints)}')
        logger.debug(f'Deffender\'s probabilities:')
        logger.debug(
            f'{list(str(prob.varValue) + " " for prob in probs_p_two)}')

        return v.varValue, [abs(c.pi) for c in constraints], [prob.varValue for
                                                              prob in
                                                              probs_p_two]
