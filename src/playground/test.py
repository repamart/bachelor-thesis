import torch


if __name__ == "__main__":
    print("Hello world")

    # Initial input value
    x = torch.tensor([0.1], requires_grad=True)

    learning_rate = 0.5e-1
    optimizer = torch.optim.Adam([x], lr=learning_rate)

    # Adjusting input x to minimize function in x using pytorch optim package
    for i in range(500):
        print(f'Value of x={x.item()}')
        loss = torch.add(torch.pow(x, 2),torch.mul(torch.sin(2*x),torch.div(6, x)))

        optimizer.zero_grad()

        loss.backward()

        optimizer.step()
