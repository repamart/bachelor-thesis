from src.game import Game
from src.config import RootConfig


if __name__ == "__main__":
    # Create default config for the game and solve it
    conf = RootConfig()

    game = Game(conf)
    game.solve_game()

    input('Pres enter to exit')
