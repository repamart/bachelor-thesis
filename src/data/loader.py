from os.path import dirname
from pathlib import Path

import pandas as pd
import numpy as np


def np_matrix_from_scored_csv(file_name: str, label: int):
    df = pd.read_csv(Path(dirname(__file__)) / Path('scored')/Path(file_name))

    matrix = df.values
    labels = np.full(len(matrix), label)
    return matrix, labels


if __name__ == "__main__":
    a = np_matrix_from_scored_csv('test.csv', 0)
    print(a)
