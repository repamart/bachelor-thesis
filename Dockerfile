ARG  IMAGE=python3.7-cuda:latest
FROM ${IMAGE} as common

ENV APP_DIR /app
WORKDIR ${APP_DIR}
ARG DEVICE=cuda
ENV device ${DEVICE}

# precompile wheels ----------------------------------------------------
FROM common AS install

COPY --from=wheelcache-bp:latest /wheels /wheels/

WORKDIR /src
COPY requirements.txt /src/
RUN pip3.7 install wheel
RUN pip3.7 wheel   -r requirements.txt --find-links=/wheels --wheel-dir=/wheels
RUN pip3.7 install -r requirements.txt --find-links=/wheels

# store wheels ---------------------------------------------------------
FROM scratch AS wheelcache
COPY --from=install /wheels /wheels/

# image ----------------------------------------------------------------
FROM common AS image

COPY --from=install /usr/local /usr/local

RUN mkdir -p /home/ignac/experiments/learning_epochs \
             /home/ignac/experiments/lp_blocking \
             /home/ignac/experiments/lp_latency \
             /home/ignac/experiments/game_theory_model_latency \
             /home/ignac/experiments/game_theory_model_blocking

COPY src/ ${APP_DIR}/src/
COPY setup.py start_experiments.sh ${APP_DIR}/

CMD ["./start_experiments.sh"]
