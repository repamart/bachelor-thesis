#!/bin/bash

# set -e

export PYTHONPATH=/app

echo "Going to run learning epochs"
python3.7 src/experiments/learning_epochs/learning_epochs.py

# echo "Going to run blocking GT model"
# python3.7 src/experiments/game_theory_blocking/run_blocking_model.py

# echo "Going to run latency GT model"
# python3.7 src/experiments/game_theory_latency/run_latency_model.py

# echo "Going to run lp blocking"
# python3.7 src/experiments/lp_blocking/lp_blocking.py

# echo "Going to run lp latency with big latencies"
# python3.7 src/experiments/lp_latency/lp_latency.py

# echo "Going to run lp latency with small latencies"
# python3.7 src/experiments/lp_latency/lp_latency.py small

echo "All experiments are done..."

# Just to keep container running and it does not exit
tail -f setup.py