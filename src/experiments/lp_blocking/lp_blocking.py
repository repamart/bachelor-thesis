import functools
import itertools
import operator
import os
import pickle
import sys
import time
from collections import Counter
from dataclasses import dataclass
from typing import List, Tuple

import numpy as np
import pulp

from src.data.loader import np_matrix_from_scored_csv

# Global "config" variables
features_num = 2
density = 2  # 1 is low | 2 is high

benign_ratio = 1
data_file = 'normal_distribution_experiments.csv'

run_times = 1
legacy_folder = f'/home/ignac/experiments/lp_blocking/' \
                f'feat{features_num}_br{benign_ratio}_dens{density}_{run_times}times'


@dataclass
class Result:
    zero_sum_game_value: float
    fp_part: float
    almost_sum_game_attacker: float
    almost_sum_game_defender: float

    attacker_actions: List[Tuple[float]]
    attacker_probs: List[float]

    defender_actions: List[Tuple[float]]
    defender_probs: List[List[float]]

    time_taken: List[float]


def create_actions() -> List:
    if density == 1:
        one_axis = np.linspace(0, 1, 11)  # [0, 0.1, 0.2, ..., 0.9, 1]
    elif density == 2:
        one_axis = np.linspace(0, 1, 101)  # [0, 0.01, 0.02, ..., 0.99, 1]
    else:
        print(f'Density is not high (2) neither low (1). It is {density}')
        sys.exit(5)

    one_axis = np.round(one_axis, density)  # To get rid of bad double precision
    generator = itertools.product(*itertools.repeat(one_axis, features_num))
    return list(generator)


def get_data() -> dict:
    benign, _ = np_matrix_from_scored_csv(data_file, 0)

    # Round to proper density
    benign = list(
        map(lambda x: tuple(map(lambda y: round(y, density), x)), benign))

    benign_data_prob = Counter(benign)
    for key, val in benign_data_prob.items():
        benign_data_prob[key] = val / len(benign)
    return benign_data_prob


def utility(attacker_action: Tuple):
    return functools.reduce(operator.mul, attacker_action, 1)


def solve_with_lp(actions_attacker: List,
                  actions_defender: List,
                  benign_data_prob: dict):
    print('Going to solve with LP')

    # Create LP problem
    m = pulp.LpProblem("Zero sum game", pulp.LpMinimize)

    # Minimizing value "v"
    v = pulp.LpVariable("v")
    m += v

    # Player two probabilities vector
    print("Defining defenders probabilities...")
    probs_defender = []
    for action in actions_defender:
        probs_point = pulp.LpVariable(f'p({action[0]},{action[1]})', 0, 1)
        probs_defender.append(probs_point)

    fp_cost = 0
    num_of_fp = 0
    for action, prob in zip(actions_defender, probs_defender):
        cur_fp = prob * benign_data_prob[action]
        num_of_fp += cur_fp
        fp_cost += cur_fp * benign_ratio

    print("Defining main constraint...")
    constraints = []
    for i in range(len(actions_attacker)):
        sum = [fp_cost, (1 - probs_defender[i]) * utility(actions_attacker[i])]
        constraints.append(pulp.lpSum(sum) <= v)
    for c in constraints:
        m += c

    print("Ok, let's solve now...")
    m.solve()

    print(f'LP solved')
    print(f'Value of the zero sum game: {v.varValue}')
    print(f'Found solution: {pulp.LpStatus[m.status]}')
    print(f'Attacker\'s probabilities:')
    for i in range(len(constraints)):
        prob = abs(constraints[i].pi)
        if prob == 0: continue
        print(f'Action {actions_attacker[i]} -> {prob}')

    print(f'Deffender\'s probabilities to block:')
    for i in range(len(probs_defender)):
        print(f'Action {actions_attacker[i]} -> {probs_defender[i].varValue}')

    # Return value and probabilities
    return v.varValue, [abs(c.pi) for c in constraints], \
           [prob.varValue for prob in probs_defender]


def calc_fals_positives(p2_actions, p2_probs, benign_probs):
    fp_cost = 0
    for action, prob in zip(p2_actions, p2_probs):
        if prob is None: prob = 0
        fp_cost += prob*benign_probs[tuple(action)]
    return fp_cost * benign_ratio


def main():
    # Get actions
    attacker_actions = create_actions()
    defender_actions = create_actions()

    # load data, round data and make occurrences dict
    benign_probs = get_data()

    game_val, attacker_probs, probs_defender = None, None, None

    time_takens = []
    for i in range(run_times):
        print(f'Run lp blocking for {i + 1}. time')
        start = time.time()
        res = solve_with_lp(attacker_actions, defender_actions, benign_probs)
        game_val, attacker_probs, probs_defender = res
        time_takens.append(time.time() - start)

    fp_part = calc_fals_positives(defender_actions, probs_defender, benign_probs)

    final_experiment = Result(game_val,
                              fp_part,
                              game_val - fp_part,
                              -game_val,
                              attacker_actions, attacker_probs,
                              defender_actions, probs_defender,
                              time_takens)
    print("Experiment done.")

    # ----------------- Store final data -----------------
    experiment_data_file = f'{legacy_folder}/data'
    print(f'Storing final data to {experiment_data_file} file.')
    with open(experiment_data_file, 'wb') as file:
        pickle.dump(final_experiment, file)
    print('File saved')

    # ------- Do backup of configuration -----------------
    backup_object = {
        'features_num': features_num,
        'density': density,
        'benign_ratio': benign_ratio,
        'data_file': data_file,
        'run_times': run_times,
        'legacy_folder': legacy_folder
    }
    backup_conf_file = f'{legacy_folder}/backup_conf'
    print(f'Storing backup conf to {backup_conf_file} file.')
    with open(backup_conf_file, 'wb') as file:
        pickle.dump(backup_object, file)
    print('File saved')

    print('Recapitulation:')
    print(f'Final value {game_val}')
    print(backup_conf_file)


if __name__ == "__main__":
    # Create base folder if it does not exist
    if not os.path.exists(legacy_folder):
        print(f'Creating base dir {legacy_folder}')
        os.mkdir(legacy_folder)

    log_file = f'{legacy_folder}/log'
    with open(log_file, 'a') as log:
        sys.stdout = log
        main()
    # main()

