from pathlib import Path
from typing import Tuple, List, Callable
import matplotlib.pyplot as plt
import random
import attr
import numpy as np


@attr.s
class Cluster:
    center: List[float] = attr.ib()
    radius: float = attr.ib()
    num_of_points: int = attr.ib()


class Synthesizer:
    """
    Synthesizer generates synthetic data which might be used for testing.

    :arg features_num specifies dimension of a point space
    """
    def __init__(self, features_num: int):
        self._features_num: int = features_num
        self._clusters: List[Cluster] = []
        self.points: List[List[float]] = []
        self._generated = False

    def add_cluster(self, cluster: Cluster):
        """
        Add points to a space given the cluster
        :param cluster: definition of a cluster
        """
        if cluster.radius < 0 or cluster.radius > 1:
            raise ValueError('Radius must be in range [0;1]')
        if len(cluster.center) != self._features_num:
            raise ValueError(f'Center must be element of a space R^'
                             f'{self._features_num}')
        self._clusters.append(cluster)

    def add_cluster_around_2Dfunc(self, func: Callable, radius: float, num=40):
        """
        Add some points around a function.
        Possible only for space R^2
        """
        x_axis = np.linspace(0, 1, num)
        tmp = int(radius * 100)
        for x in x_axis:
            y = func(x)
            if y < 0 or y > 1:
                continue

            y = np.random.normal(loc=y, scale=radius/2, size=1)[0]
            x = min(max(x, 0), 1)
            y = min(max(y, 0), 1)
            self.points.append([x, y])

    def generate(self):
        self._generated = True

        for cluster in self._clusters:
            tmp = int(cluster.radius*100)
            for iter in range(cluster.num_of_points):
                point = []
                for i in range(self._features_num):
                    coord = random.randint(-tmp, tmp)/100
                    coord = min(max(cluster.center[i] + coord, 0), 1)
                    point.append(coord)
                self.points.append(point)

    def add_raw_data(self, arr):
        self.points.extend(arr)

    def plot2D(self):
        """
        Show generated data in a 2D chart
        Possible only for space R^2

        Data needs to be generated at first with 'generate' function
        """
        print('Let\'s plot result')
        if not self._generated:
            raise RuntimeError('Trying to plot while points are not generated')
        if self._features_num != 2:
            raise ValueError(f'Trying to plot in 2D, but there are '
                             f'{self._features_num} features')
        plt.xlim(0, 1)
        plt.ylim(0, 1)
        xs = list(map(lambda x: x[0], self.points))
        ys = list(map(lambda x: x[1], self.points))

        plt.scatter(xs, ys, c='red', s=1.)
        plt.show()
        print('done')

    def save_to_file(self, path: Path):
        """
        Save generated data to a file.
        Data needs to be generated at first with 'generate' function
        """
        if not self._generated:
            raise RuntimeError('Trying to save while points are not generated')

        with open(path, 'w', encoding='utf-8') as file:
            initial_line = ','.join([f'initial{i}' for i in range(0, self._features_num)])
            file.write(f'{initial_line}\n')

            for point in self.points:
                line = ','.join([str(c) for c in point])
                file.write(f'{line}\n')


if __name__ == "__main__":
    synt = Synthesizer(3)

    # Generate 3D normal distribution data
    arr = np.random.multivariate_normal([0, 0, 0], [[2, 1, 1], [1, 5, 1],
                                                    [1, 1, 2]], 10000)
    arr = arr - np.min(arr, axis=0)
    m = np.max(arr, axis=0)
    arr = arr / (m + (m*0.2))
    synt.add_raw_data(arr)


    # synt = Synthesizer(2)

    # Generate 2D normal distribution data
    # arr = np.random.multivariate_normal([0, 0], [[2, 1], [1, 1]], 10000)
    # arr = arr - np.min(arr, axis=0)
    # m = np.max(arr, axis=0)
    # arr = arr / (m + (m*0.2))
    # synt.add_raw_data(arr)

    # synt.add_cluster_around_2Dfunc(lambda x: 0.8/(15*(x+.05)), 0.25, 15000)

    synt.generate()
    # synt.plot2D()
    synt.save_to_file(Path('scored/normal_distribution_3D_experiments.csv'))
