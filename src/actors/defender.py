from typing import List

from src.config import ModelConfig
from src.data.loader import np_matrix_from_scored_csv
from src.neural_networks.network import NeuralNetwork, FormattedData, BenignData
import numpy as np
import logging

logger = logging.getLogger(__name__)


def prepare_benign_data(raw_x_data) -> BenignData:
    logger.debug('Let\'s prepare benign data. Taking only unique records.')
    unique, counts = np.unique(raw_x_data, axis=0, return_counts=True)
    raw_y_label = np.zeros(len(unique))
    logger.debug('Data preparation done.')
    return BenignData(unique, counts, raw_y_label)


class Defender:
    def __init__(self, model_conf: ModelConfig):
        self.conf = model_conf.defender_conf
        self.features_count = model_conf.features_count
        self.attacker_utility = model_conf.attacker_utility

        # Prepare benign data
        raw_x, _ = np_matrix_from_scored_csv(self.conf.benign_data_file_name, 0)
        self.benign_data = prepare_benign_data(raw_x)

    def get_best_response(self, att_actions: List, att_probs: List) -> NeuralNetwork:
        logger.info('Defender searching for response')
        # Take only attacker actions which are played with non zero probability
        non_zero_p = np.where(np.asarray(att_probs) != 0)

        attack_x = np.asarray(att_actions)[non_zero_p]
        attack_probs = np.asarray(att_probs)[non_zero_p]
        attack_y = np.ones(len(attack_x))
        attack = FormattedData(attack_x, attack_probs, attack_y)

        logger.debug('Let\'s train new best NN with this malicious data:')
        logger.debug(f'{attack_x}\n')

        best_nn = self._train_nn(attack)
        for _ in range(1, self.conf.number_of_nn_to_train):
            new_nn = self._train_nn(attack)
            self._log_creation(new_nn, best_nn)
            if new_nn.final_loss < best_nn.final_loss:
                best_nn = new_nn

        logger.info(f'Defender found new br neural network with id {best_nn.id}')
        return best_nn

    def _log_creation(self, new_nn, best_nn):
        if new_nn.final_loss < best_nn.final_loss:
            logger.debug(f'Found better nn. Old|New value: '
                         f'{best_nn.final_loss} | {new_nn.final_loss}')
        else:
            logger.debug(f'The previous nn was better, dif: '
                         f'{new_nn.final_loss - best_nn.final_loss}')

    def get_initial_action(self) -> NeuralNetwork:
        non_attack = self._get_empty_attack()
        return self._train_nn(non_attack)

    def _get_empty_attack(self) -> FormattedData:
        non_features = np.ndarray((0, self.features_count))
        non_1d = np.array([])
        return FormattedData(non_features, non_1d, non_1d)

    def _train_nn(self, attack: FormattedData) -> NeuralNetwork:
        # Initialize the model
        network = NeuralNetwork(self.features_count, self.conf.nn_conf)
        network.set_data(self.benign_data, attack)
        network.train()
        return network

    def does_br_exists(self, best_response: NeuralNetwork, value):
        it_does = value - best_response.final_loss < self.conf.defender_epsilon
        if it_does:
            logger.debug('This neural network already exists')
        else:
            logger.debug('This neural network does not exist yet')
        return it_does
