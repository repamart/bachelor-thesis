\documentclass{article}

% \usepackage[utf8x]{inputenc}
%\usepackage{caption}
%\usepackage{subcaption}
% \usepackage{fancyhdr}
% \usepackage{amssymb}
% \usetikzlibrary{trees}

\usepackage{algorithmic}
\usepackage{algorithm}
\usepackage{todonotes}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{titlepic}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{array}
\usepackage{pdflscape}
\usepackage{adjustbox}
\usepackage{amsfonts}
\usepackage{hyperref}
\usepackage{enumerate}


\usepackage{biblatex}
\addbibresource{references.bib}

\def\BState{\State\hskip-\ALG@thistlm}

% Setting penalty for breaking line or page after[before] first[last] line of
% paragraph
\clubpenalty=10000
\widowpenalty=10000

% Setting behaviour and looks of links
\hypersetup{
        colorlinks,
        citecolor=black,
        filecolor=black,
        linkcolor=black,
        urlcolor=black
}

\begin{document}
\input{titlepage}
\tableofcontents
\newpage

\section{Introduction}
\subsection{Domain Name System (DNS)}
Domain Name System (shortly DNS) provides way to match human readable hostname
to its related IP address. The reason it exists is so people can remember
 user-friendly hostname instead of hardly remembered numeric IP addresses.

The domains' namespace has a hierarchical structure. Domain names are organized
in subordinate levels of the DNS root domain, which is nameless. The first
level is a top-level domain (e.g. com, cz, net, org, edu, ...) followed by the
second level, third level and so on. Each level may contain up to 63 characters,
but the full domain name may not exceed the length of 253 characters in its
textual representation.

\begin{center}
  \begin{figure}[h]
    \includegraphics[width=1\textwidth]{img/dns_tree.png}
    \caption{A hierarchical DNS structure}
    \label{fig:dnsTreeStructure}
  \end{figure}
\end{center}

It would be crazy to think that one centralized database
would be able to handle the whole world's DNS traffic, thus every domain has
a domain name server maintaining the records in its server's database.

The general process of resolving an IP address starts when DNS client (for
example browser) issues a DNS request or DNS address lookup, providing
a hostname such as “example.test.com”. Then the so called DNS resolver starts
asking for an IP address from the root domain moving down the hiearchy until it
reaches the authoritative server for desired hostname "example.test.com" and
obtains response with the IP address and some additional information.

\subsubsection{Packet structure}
The DNS messages are encapsulated over UDP or TCP using port 53. The same
message format is used for all exchanges between client and servers.

\begin{table}[h] % [h] solves the issue of putting table above subsection title
  \centering
  \begin{tabular}{ |c|c|c| }
    \hline Identification & Control & Question count \\
    \hline Answer count & Authority count & Additional count \\
    \hline \multicolumn{3}{|c|}{Question} \\
    \hline \multicolumn{3}{|c|}{Answer} \\
    \hline \multicolumn{3}{|c|}{Authority} \\
    \hline \multicolumn{3}{|c|}{Additional} \\
    \hline
  \end{tabular}
  \caption{Packet format \cite{dnspacketstructure}}
  \label{table:dnsPacketFormat}
\end{table}

\subsubsection{Exfiltrating data}
Unfortunately DNS is not only used for purpose it was made for. Imagine a
situation when malicious attacker somehow got access to a foreign server and
compromised private data. DNS might help him to stealthily smuggle data out to
his own server. All he needs to do is setup his own domain (for example
"hacker.test.xyz"), issue a DNS query from compromised server providing a
hostname similar to "stolenPassword1.hacker.xyz" and his customized domain
network server would just save the stolen data. Easy and elegant.

\begin{center}
    \includegraphics[width=1\textwidth]{img/exfiltrating_data.png}
\end{center}

Since DNS protocol is a core component of the Internet protocol suite, it's
almost never blocked by firewall and attacker is able to setup a "dns tunnel".
Also DNS traffic is rarely monitored so it might be too late when the data leak
is discovered.

\subsection{Motivation}
Security companies often offer a way to detect anomalies in network traffic such
as for example exfiltrating data via dns requests, scanning network machine's port.
The question coming up is what to do after the detection. Generally the most common
approach is to block the IP address, however this way the attacker might change his IP
address, which means our detection effort is lost. Also false positives are quite
expensive, thus it's hard to find proper blocking threshold so benign users are not
restricted.

The goal of this project is then to propose a model (with regards all possible
deffender's actions, such as ratelimiting, blocking or redirecting to
honeypots) solving this issue and implement initial skeleton to get first
results.

\section{Solution}
I chose a game theoretical approach to solve the issue. Firstly I try to model
a game considering only blocking as a defender's action which I would later
expand to include also ratelimiting and redirecting actions.

To find anomaly dns request we need to properly split a space of attacker's
actions into regions classifying the requests. Instead of choosing a linear
separators or plotting some kinds of spheres I choose more flexible approach - neural
networks which are able to approximate any continuous functions (Universal
approximation theorem).
See examples in $\mathbb{R}^2$.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=0.3\textwidth]{img/linear_regions.png}
    \includegraphics[width=0.3\textwidth]{img/sphere_regions.png} \\
  \end{center}
  \begin{center}
    \includegraphics[width=0.70\textwidth]{img/network_regions.png}
  \end{center}
\end{figure}

Every neural network classifies request either as malicious or benign and
determines probability to block the request in case of classified as malicious.

I will search for Nash Equilibrium. It can be proven that Nash
Equilibrium must exist if we allow mixed strategies (Nash's Existence Theorem)\cite{gtshoham}.

Every defender's mixed strategy of neural networks defined as above can be without
loss of generality transformed to mixed strategy of neural networks which determine
the probability $1$ to block the request if it's classified as malicious.

Let's see an example, where $NN_p$ represents neural network with blocking
probability $p$:

\begin{center}
\begin{equation*}
\begin{aligned}[c]
  1.NN_{0.6} \text{ played with prob. } 0.1 \\
  2.NN_{0.7} \text{ played with prob. } 0.6 \\
  3.NN_{0.2} \text{ played with prob. } 0.3 \\
\end{aligned}
\qquad \leftrightarrow \qquad
\begin{aligned}[c]
  1.NN_{1} &\text{ played with prob. } 0.1 \cdot 0.6=0.06 \\
  2.NN_{1} &\text{ played with prob. } 0.6 \cdot 0.7=0.42 \\
  3.NN_{1} &\text{ played with prob. } 0.3 \cdot 0.2=0.06 \\
  4.NN_{1} &\text{ classifying all requests as benign played with } \\
           &\text{ prob } 1-(0.06+0.42+0.06)
\end{aligned}
\end{equation*}
\end{center}

It means that if I include neural network classifying all requests as benign in my
future model I might only consider neural networks with blocking probability
equals one to ease solving the game.

\subsection{Initial model}
The game is modeled as a zero sum normal form game $G$, where $G = \{N, A, u\}$:

\begin{itemize}
  \item $N = \{\text{attacker}, \text{defender}\}$ is set of players
  \item $A = A_{attacker} \times A_{defender}$ is an action profile
    \begin{itemize}
      \item $A_{attacker} = \{x | x \in \mathbb{R}^n\}$ is a set of attacker's
      actions, where $n$ is number of features used (see below)
      \item $A_{defender}$ is set of neural networks, which are able to classify
      each attacker's action as either malicious (block) or benign (do nothing)
    \end{itemize}
  \item $u$ is an Utility function or Payoff function $f: A_{attacker} \times
    A_{defender} \mapsto \mathbb{R}$
    It's possible to experiment with definition of this function, but I have used
    the following one assuming the attacker wants to maximize every feature
    \begin{equation}
         f(a_a, a_d) = \begin{cases}
              \prod_{i=1}^{n} a_{ai} & \text{if $a_d$ classifies $a_a$ as benign}\\
                        0            & \text{if $a_d$ classifies $a_a$ as malicious}
                      \end{cases}
    \end{equation}
\end{itemize}

Defender's optimal strategy for the game defined as this would be neural network
blocking every request, which is definitely wrong so I need to include false
positive constraint to prevent this. Every defender's mixed strategy
must satisfy constraint of false positives being $ \le FP_{constraint}$.
How precisely I include $FP_{constraint}$ will be seen in following sections.

Alternatively there is an option to cover false positives as a cost during
neural networks training phase. Unfortunately there was no time to examine this
path, hence it remains for me as a future exploration.

\subsubsection{Features}
Each attacker action is a vector $f \in \mathbb{R}^n$ which consists of $n$
features. Each feature is some property of DNS request, but the selection of
features heavily affects the final result. Initially I'm using only 2 features so
the results might be smoothly visualized: entropy and length of the request.
Utility function used in game $G$ depends on the chosen features. In my case
the attacker wants to maximize both of my features, that's why I've used such
definition.

\begin{algorithm}
\caption{Entropy}
\begin{algorithmic}
  \REQUIRE request
  \STATE $e \leftarrow 0$
  \STATE $occurrence \leftarrow emptyMap$, initial value 0
  \FORALL{char in request}
    \STATE $occurrence$[char]$ \leftarrow occurrence$[char]$+1$
  \ENDFOR
  \FORALL{key, value in $occurrence$}
    \STATE $p \leftarrow \dfrac{value}{length(request)}$
    \STATE $ e \leftarrow e - (p \cdot \log_2 p)$
  \ENDFOR
  \RETURN $e$
\end{algorithmic}
\end{algorithm}

Other features of dns request to consider for future development might be bigrams, trigrams, occurrence of unusual letters or number of digits.

\subsection{Solving the game}
To find Nash Equilibrium of the game I use double oracle algorithm \cite{doubleoraclepaper}. Double oracle basically works in following steps:

\begin{algorithm}
\caption{Double oracle}
\begin{algorithmic}
  \STATE $a_{p1} \leftarrow$ \text{array with 1 random player1 action}
  \STATE $a_{p2} \leftarrow$ \text{array with neural network classifying all
                                   requests as benign}
  \LOOP
    \STATE $probs_{p1}, probs_{p2} \leftarrow$ \text{ solve\_game($a_{p1}, a_{p2}$)}
    \STATE $best\_response_{p1} \leftarrow$ \text{ best response of player1}
    \STATE $best\_response_{p2} \leftarrow$ \text{ best response of player2}
    \IF{$best\_response_{p1} \in a_{p1} \textbf{ AND } best\_response_{p2} \in a_{p2}$}
      \RETURN $probs_{p1}, probs_{p2}$
    \ELSE
      \STATE $a_{p1}$\text{.add($best\_response_{p1}$)}
      \STATE $a_{p2}$\text{.add($best\_response_{p2}$)}
    \ENDIF
  \ENDLOOP
\end{algorithmic}
\end{algorithm}

The algorithm rises other questions:
\begin{enumerate}[1.]
  \item How to compare 2 neural networks for equality?\cite{gangs} \\
    Given the utility function $u$ and 2 neural networks $nn_1$ and $nn_2$ I say
    that these neural networks are similar enough to be seen as equal if and only if
    \begin{equation*}
      \mid u(a_i, nn_1) - u(a_i, nn_2) \mid < \varepsilon \qquad \forall a_i \in a_{p1}
      \qquad \varepsilon \in \mathbb{R}^+
    \end{equation*}

  \item How to search for best responses for each player? \\
    Actually finding the best response for an attacker is one of my topics for
    future research. For now I have to traverse the whole space of attacker's
    actions and choose the best one, which is totally unacceptable when using
    number of features greater than 2.

    As a best response for defender I choose new neural network trained with
    my benign data set (see \hyperref[subsubsec:data]{data section}) and sample
    of malicious data represented by attacker's current mixed strategy.

  \item How to solve the game given the set of possible actions?? \\
    Problem of solving the game in each iteration of the double oracle algorithm
    can be transformed to a linear program and use any linear program solver to get
    optimal mixed strategy for each player.

    Given $m$ attacker actions
    $a=(a_1,\hdots,a_m)$, $n$ defender's actions $d=(d_1, \hdots, d_n)$,
    number of false positive classification $f_i$ by action $d_i$ when tested with
    our benign data set, utility function $u$ and constant $FP_{allowed}$ determining total number of false positives allowed, then the linear program looks as follow:

  \begin{equation*}
    \begin{aligned}
      & \text{minimize}   & & v \\
      & \text{s.t.}       & &
      \begin{bmatrix}
        1^T \cdot [u(a_1, d_1)\cdot P_{1}, \hdots, u(a_1, d_n)\cdot P_{n}] \\
        \vdots \\
        1^T \cdot [u(a_m, d_1)\cdot P_{1}, \hdots, u(a_m, d_n)\cdot P_{n}]
      \end{bmatrix}       & & \le v \\
      &                   & & F^T \cdot P & & \le FP_{allowed} \\
      &                   & & 1^T \cdot P & & = 1 \\
      &                   & & F \in \mathbb{R}^n \quad P \in \mathbb{R}^n \quad
                                v \in \mathbb{R} \quad P \ge 0
    \end{aligned}
  \end{equation*}
  $P$ represents a defender's mixed strategy meaning $p_i$ is a probability for
  defender to play the action $d_i$. Finally attacker's mixed strategy is
  represented by absolute value of dual variables from dual problem.
\end{enumerate}

\subsubsection{Data}
\label{subsubsec:data}
The only data I need to test my model are data representing benign dns requests.
Malicious data are represented with dynamic attacker's actions. Unfortunately I
did not find or get any public dataset containing 100\% only benign data, that's
why I implemented simple synthesizer and created my own synthetic data to see
functionality of my implementation/model.

\subsection{Implementation}
Entire details can be seen in my source code (well commented), thus
feel free to check and test my Python3.6 implementation, which can be found at
CTU FEE gitlab. See \url{https://gitlab.fel.cvut.cz/repamart/bachelor-thesis}.

\subsection{Results}
Finally in this subsection I would present result of my implementation run with
synthetic benign data (figure \ref{fig:synteticdata}), what's more, I used modified
utility function $u$ (equation \ref{eq:modifiedutility}), which has its maximum
value in the middle $(0.5, 0.5)$ and gets lower the further you get so it complies
with the data. I am using two features for smoother visualization.


\begin{center}
  \begin{figure}[h]
    \includegraphics[width=1\textwidth]{img/syntetic_data.png}
    \caption{Synthetic benign data}
    \label{fig:synteticdata}
  \end{figure}
\end{center}

\begin{equation} \label{eq:modifiedutility}
  u(a_a, a_d) =
  \begin{cases}
    \max(1- \Vert(0.5,0.5)-(a_{a1}, a_{a2})\Vert \cdot 2, 0) & \text{if $a_d$
    classifies $a_a$ as benign}\\
    0            & \text{if $a_d$ classifies $a_a$ as malicious}
  \end{cases}
\end{equation}

My implementation found a Nash Equilibrium after 33 iterations of double oracle
algorithm and this is the result. The attacker's final mixed strategy contains
8 actions, which means there are 8 attacker's actions with non zero probability
of being played.

\begin{table}[h] % [h] solves the issue of putting table above subsection title
  \centering
  \begin{tabular}{ c|c }
    Point        & Probability \\ \hline
    (0.59, 0.30) & 0.37417422  \\
    (0.30, 0.58) & 0.15655936  \\
    (0.48, 0.62) & 0.12617677  \\
    (0.38, 0.65) & 0.10336114  \\
    (0.63, 0.62) & 0.098506108 \\
    (0.34, 0.64) & 0.077516031 \\
    (0.62, 0.43) & 0.052884573 \\
    (0.60, 0.32) & 0.010821798 \\
  \end{tabular}
  \caption{Attacker's actions with non zero probability}
  \label{table:attackeractions}
\end{table}

\begin{center}
  \begin{figure}[h]
    \includegraphics[width=0.9\textwidth]{img/attacker_actions.png}
    \label{fig:attackeractions}
  \end{figure}
\end{center}

Defender's final mixed strategy consists of 11 actions (neural networks).
Heat map of their classification can be seen at figure \ref{fig:heatmap}. Light
green color denotes no blocking at all while the light green signifies 100\%
blocking. Classification of each defender's action can be seen below separately.

\begin{center}
  \begin{figure}[h]
    \includegraphics[width=0.9\textwidth]{img/deffender_heatmap.png}
    \label{fig:heatmap}
  \end{figure}
\end{center}

\begin{figure}[h!]
  \begin{tabular}{ccc}
      \includegraphics[width=0.3\textwidth]{img/nn1.png} &
      \includegraphics[width=0.3\textwidth]{img/nn2.png} &
      \includegraphics[width=0.3\textwidth]{img/nn3.png} \\

      \includegraphics[width=0.3\textwidth]{img/nn4.png} &
      \includegraphics[width=0.3\textwidth]{img/nn5.png} &
      \includegraphics[width=0.3\textwidth]{img/nn6.png} \\

      \includegraphics[width=0.3\textwidth]{img/nn7.png} &
      \includegraphics[width=0.3\textwidth]{img/nn8.png} &
      \includegraphics[width=0.3\textwidth]{img/nn9.png} \\

      \includegraphics[width=0.3\textwidth]{img/nn10.png} &
      \includegraphics[width=0.3\textwidth]{img/nn11.png} \\
  \end{tabular}
  \caption{Visualisation of deffender's final mixed strategy}
\end{figure}

As we can see the results are not bad. On the other hand for example the
asymmetry that can be seen at defender's actions can be surprising. I assume
it's caused by randomization during creating synthetic benign data. Even though
the synthetic benign data set looks symmetric there are slight differences - e.g.
in number of points in each cluster.

If you would like to see used figures in an original size or to see more details
about the result, you can see complete details at implementation repository in
\textit{results/syntetic1} directory.

\newpage
\section{Conclusion}
In my opinion I succeeded in proposing and implementing initial model, however
I managed to cover only blocking as a defender's strategy and so adding more strategies
for defender is going to be my future focus. Also the scope of future exploration
covers using false positive costs instead of constraints, better way of finding
attacker's actions instead of traversing the whole space of attacker's actions and lastly
experiment with more features actually used.

Finally to fully utilize the model I need to come up with real dns data set so
the results can be actually useful.

\newpage
\printbibliography

\end{document}
