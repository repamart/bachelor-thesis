from typing import Callable

import attr

from src.utility import *


@attr.s
class NeuralNetworkConfig:
    # Number of epochs in a neural network training phase
    epochs: int = attr.ib(default=10000)

    # Learning rate for Adam optimiser
    learning_rate = 0.1e-3
    # learning_rate = 0.1e-2
    # learning_rate = 0.5e-2

    # Size of a benign data batch used in each epoch of training
    batch_size = 1000

    # Loss function used for training
    loss_function: Callable = attr.ib(init=False)


@attr.s
class DefenderConfig:
    # New actor's action is considered already played in the game if difference
    # between utility of new action and nash equilibrium value of zero-sum game
    # is less than this value
    defender_epsilon: float = attr.ib(default=1e-3)

    # This number of neural networks will be trained in each double oracle
    # iteration and the best one will be considered as a best response
    number_of_nn_to_train: int = attr.ib(default=1)

    # conf of neural networks
    nn_conf: NeuralNetworkConfig = attr.ib(default=NeuralNetworkConfig())

    # Name of .csv file in src/data/scored directory with scored data which will
    # be used as benign data in neural network training phase
    benign_data_file_name: str = attr.ib(default='normal_distribution_experiments.csv')
    # benign_data_file_name: str = attr.ib(default='test.csv')


@attr.s
class AttackerConfig:
    # If set to True gradient descent within pytorch.optim package
    # is used to find attacker's best response
    # If set to False, attacker actions are discrete and the whole space is
    # traversed to find best response. Example for R^2:
    # [(.0,.01),(.0,.02),...,(.1,.1)]
    use_gradient_descent: bool = attr.ib(default=True)

    # New actor's action is considered already played in the game if difference
    # between utility of new action and nash equilibrium value of zero-sum game
    # is less than this value
    epsion: float = attr.ib(default=1e-3)

    # Number of random tries to find attacker action using gradient descent.
    # The one with best final loss value would be chosen.
    # Attention. Used only when use_gradient_descent is set to True!
    tries_for_best_response: int = attr.ib(default=264)

    # Learning rate for optimiser which updates attacker action while searching
    # for best response using gradient descent
    # Attention. Used only when use_gradient_descent is set to True!
    learning_rate = 0.5e-4

    # Number of iterations used to update gradient descent while searching for
    # best response
    # Attention. Used only when use_gradient_descent is set to True!
    epochs = 2000


@attr.s
class ModelConfig:
    # Use blocking or latency model?
    use_blocking_model: bool = attr.ib(default=False)

    # Number of features
    features_count: int = attr.ib(default=2)

    # Attacker
    attacker_conf: AttackerConfig = attr.ib(default=AttackerConfig())

    # Defender
    defender_conf: DefenderConfig = attr.ib(default=DefenderConfig())

    # i_a, used only for latency
    i_a: int = attr.ib(default=1)

    # i_d, used only for latency
    i_d: int = attr.ib(default=2)

    # malicious : benign ratio in datatests
    benign_ratio: int = attr.ib(default=5)

    # Function to calculate utility for attacker given the actions
    # f: List[float], NeuralNetwork -> float
    attacker_utility: Callable = attr.ib(init=False)

    # Attacker utility function using torch tensors with gradient property
    # Used for attacker to find best response via gradient descent
    attacker_torch_utility: Callable = attr.ib(init=False)

    def __attrs_post_init__(self):
        if self.use_blocking_model:
            self.attacker_utility = get_blocking_attacker_utility()
            self.attacker_torch_utility = get_blocking_torch_grad_utility()
            self.defender_conf.nn_conf.loss_function = \
                get_blocking_nn_loss_function(self.benign_ratio)

        else:
            self.attacker_utility = get_latency_attacker_utility(self.i_a)
            self.attacker_torch_utility = get_latency_torch_grad_utility(self.i_a)
            self.defender_conf.nn_conf.loss_function = \
                get_latency_nn_loss_function(self.i_a, self.i_d, self.benign_ratio)

    def set_ia_id_benign_ration(self, i_a, i_d, benign_ration):
        self.i_a = i_a
        self.i_d = i_d
        self.benign_ratio = benign_ration
        self.__attrs_post_init__()

    def set_data_file(self, name: str):
        self.defender_conf.benign_data_file_name = name


@attr.s
class PlotterConfig:
    # Sets logger to debug level
    plot_enabled: bool = attr.ib(default=True)

    learning_epochs_plotter = attr.ib(default=False)

    # Sets logger to debug level
    output_svg_dir: str = attr.ib(default='/home/ignac/experiments/test')


@attr.s
class RootConfig:
    # Sets logger to debug level
    debug: bool = attr.ib(default=True)

    # Configuration of model used
    model_conf: ModelConfig = attr.ib(default=ModelConfig())

    # Information about plotting results
    # Unused for features != 2
    plot_conf: PlotterConfig = attr.ib(default=PlotterConfig())


if __name__ == "__main__":
    a = RootConfig()
