import logging
from typing import List
from typing import TYPE_CHECKING

import numpy as np
# Hack to avoid cycle imports while using type checking
# The TYPE_CHECKING constant is always False at runtime
import torch

if TYPE_CHECKING:
    from src.neural_networks.network import NeuralNetwork

logger = logging.getLogger(__name__)


# ---------------
#   LATENCY
# ---------------
def get_latency_attacker_utility(i_a: int):
    def attacker_utility(attacker_features: List[float], nn: 'NeuralNetwork'):
        pred = nn.predict_single_latency(attacker_features)
        return np.product(attacker_features) * (1 - pred) ** i_a

    return attacker_utility


def get_latency_torch_grad_utility(i_a: int):
    def attacker_utility(attacker_features: torch.tensor, nn: 'NeuralNetwork'):
        latency = nn.latency_predict(attacker_features, with_grad=True)
        return torch.pow(torch.add(1, -latency), i_a) \
               * torch.prod(attacker_features, dim=1)

    return attacker_utility


def get_latency_nn_loss_function(i_a: int, i_d: int, benign_ratio: int):
    def loss_function(x, latencies, real_y, probs):
        zero_sum_part = torch.sum(real_y*((1-latencies)**i_a)*torch.prod(x, dim=1)*probs)
        fp_cost = torch.mul(torch.sum((1-real_y) * probs * torch.pow(latencies, i_d)), benign_ratio)

        return torch.add(zero_sum_part, fp_cost), fp_cost

    return loss_function


# ---------------
#   BLOCKING
# ---------------
def get_blocking_attacker_utility():
    def attacker_utility(attacker_features: List[float], nn: 'NeuralNetwork'):
        pred = nn.predict_single_latency(attacker_features)
        return np.product(attacker_features) * (1 - pred)

    return attacker_utility


def get_blocking_torch_grad_utility():
    def attacker_utility(attacker_features: torch.tensor, nn: 'NeuralNetwork'):
        latency = nn.latency_predict(attacker_features, with_grad=True)
        return torch.add(1, -latency) * torch.prod(attacker_features, dim=1)

    return attacker_utility


def get_blocking_nn_loss_function(benign_ratio: int):
    def loss_function(x, latencies, real_y, probs):
        zero_sum_part = torch.sum(real_y*(1-latencies)*torch.prod(x, dim=1)*probs)
        fp_cost = torch.mul(torch.sum((1-real_y) * probs * latencies), benign_ratio)

        return torch.add(zero_sum_part, fp_cost), fp_cost

    return loss_function
