import random
from os.path import dirname
from pathlib import Path
from typing import List

from pandas import read_csv
import pandas as pd

from src.data.features import Feature, initial_line, score_query, norm_entropy, \
    norm_len, BigramCalculator


def score_benign_dns_log(features: List[Feature], debug=False) -> List[str]:
    file_path = Path('raw/dns.11-24-29-12-00-00.log')
    result = [initial_line(features, debug)]
    queries = []

    with open(file_path, 'r', encoding='utf-8') as log_file:
        for line in log_file:
            if line.startswith('#'):
                continue
            queries.append(score_query(features, line.split("\t")[8]))

    random.shuffle(queries)
    result.extend(queries)
    return result


def cache_features_wrapper(features, x, cache):
    domain = x[0]
    if domain in cache:
        return cache[domain]

    res = [feature.func(domain) for feature in features]
    cache[domain] = res
    return res


def score_single_queries(file_name: str, features: List[Feature]) -> pd.DataFrame:
    file = Path(dirname(__file__)) / Path('raw') / Path(file_name)
    df = pd.read_csv(file, header=None, sep=" ", engine="python")

    cache = {}

    columns = list(map(lambda x: x.name, features))
    apply_lambda=lambda x: pd.Series(cache_features_wrapper(features, x, cache),
                                     index=columns)
    return df.apply(apply_lambda, axis=1)


def score_csv_dns_log(features: List[Feature], debug=False) -> List[str]:
    content = read_csv('raw/b32_M250.csv')
    result = [initial_line(features, debug)]
    queries = []

    for info in content.Info:
        splited = info.split(' ')
        if splited[2] == 'response':
            continue

        queries.append(score_query(features, splited[4]))
    random.shuffle(queries)
    result.extend(queries)
    return result


def write_scored(file_name: str, result: str):
    with open(f'scored/{file_name}', 'w', encoding='utf-8') as file:
        file.write(result)


if __name__ == "__main__":
    bigram_calculator = BigramCalculator()

    features = [
        Feature('normalised entropy', norm_entropy),
        Feature('normalised length', norm_len)
    ]

    df = score_single_queries('ctu_1mil', features)
    print("Scored. Now write the file")
    write_scored('scored_ctu_1mil_2features.csv', df.to_csv())
