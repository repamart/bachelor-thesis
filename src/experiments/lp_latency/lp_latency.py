import itertools
import os
import sys
import time
import pickle
from collections import Counter
from typing import List, Tuple, Callable
from dataclasses import dataclass

import numpy as np
import pulp

from src.data.loader import np_matrix_from_scored_csv


latencies, l = None, None
try:
    latencies = sys.argv[1]
except IndexError:
    latencies = 'big'

if latencies == 'big':
    l = list(np.round(np.linspace(0, 1, 11), 2))
elif latencies == 'small':
    l = list(np.linspace(0, 1, 5))
else:
    print('Latencies is not small or big!')
    sys.exit(6)


# Global "config" variables
features_num = 2
density = 2  # 1 is low | 2 is high

i_a = 1
i_d = 3
benign_ratio = 5
data_file = 'normal_distribution_experiments.csv'

run_times = 1
legacy_folder = '/home/ignac/experiments/lp_latency' \
                f'feat{features_num}_br{benign_ratio}_dens{density}_ia{i_a}' \
                f'_id{i_d}_{run_times}times_latencies{latencies}'


@dataclass
class Result:
    zero_sum_game_value: float
    fp_part: float
    almost_sum_game_attacker: float
    almost_sum_game_defender: float

    attacker_actions: List[Tuple[float]]
    attacker_probs: List[float]

    defender_actions: List[Tuple[float]]
    defender_probs: List[List[float]]
    latencies_available: List[float]

    time_taken: List[float]


def create_actions() -> List:
    if density == 1:
        one_axis = np.linspace(0, 1, 11)  # [0, 0.1, 0.2, ..., 0.9, 1]
    elif density == 2:
        one_axis = np.linspace(0, 1, 101)  # [0, 0.01, 0.02, ..., 0.99, 1]
    else:
        print(f'Density is not high (2) neither low (1). It is {density}')
        sys.exit(5)

    one_axis = np.round(one_axis, density)  # To get rid of bad double precision
    generator = itertools.product(*itertools.repeat(one_axis, features_num))
    return list(generator)


def utility(attacker_action: Tuple, l):
    return np.prod(attacker_action) * ((1-l)**i_a)


def solve_with_lp(actions_attacker: List,
                  actions_defender: List,
                  loss: Callable,
                  benign_data_prob: dict):
    print('Going to solve with LP')

    # Create LP problem
    m = pulp.LpProblem("Zero sum game", pulp.LpMinimize)

    # Minimizing value "v"
    v = pulp.LpVariable("v")
    m += v

    # Player two probabilities vector
    print("Defining defenders probabilities...")
    probs_defender = []
    for action in actions_defender:
        probs_point = [pulp.LpVariable(f'p({action[0]},'
                                       f'{action[1]},'
                                       f'{k})', 0, 1) for k in l]
        m += pulp.lpSum(probs_point) == 1  # Probabilities sum to 1
        probs_defender.append(probs_point)

    fp_cost = 0
    for action, prob in zip(actions_defender, probs_defender):
        for j in range(len(l)):
            fp_cost += loss(l[j])*benign_data_prob[tuple(action)]*prob[j]
    fp_cost = fp_cost * benign_ratio

    print("Defining main constraint...")
    constraints = []
    for i in range(len(actions_attacker)):
        suma = [fp_cost]
        for j in range(len(l)):
            suma.append(probs_defender[i][j] * utility(actions_attacker[i], l[j]))
        constraints.append(pulp.lpSum(suma) <= v)
    for c in constraints:
        m += c

    print("Ok, let's solve now...")
    m.solve()

    print(f'LP solved')
    print(f'Value of the game: {v.varValue}')
    print(f'Found solution: {pulp.LpStatus[m.status]}')
    print(f'Attacker\'s probabilities:')
    print(f'{list(str(abs(c.pi)) + " " for c in constraints)}')
    print(f'Deffender\'s probabilities:')
    def_probs_final = []
    for probs in probs_defender:
        print(f'{list(str(item.varValue) + " " for item in probs)}')
        def_probs_final.append([c.varValue for c in probs])

    return v.varValue, [abs(c.pi) for c in constraints], def_probs_final


def calc_fals_positives(p2_actions, p2_probs, loss, benign_probs):
    fp_cost = 0
    for action, prob in zip(p2_actions, p2_probs):
        for j in range(len(l)):
            fp_cost += loss(l[j])*benign_probs[tuple(action)]*prob[j]
    return fp_cost * benign_ratio


def main():
    attacker_actions = create_actions()
    defender_actions = create_actions()

    # load data, round data and make occurrences dict
    benign = np_matrix_from_scored_csv(data_file, 0)[0]
    benign_data = np.round(benign, 2)
    benign_data_prob = Counter([tuple(x) for x in benign_data])
    for key, val in benign_data_prob.items():
        benign_data_prob[key] = val/len(benign_data)

    # Define testing loss functions
    fp_cost_loss = lambda x: x ** i_d

    game_val, attacker_probs, probs_defender = None, None, None
    time_takens = []
    for i in range(run_times):
        print(f'Run lp latency for {i+1}. time')
        start = time.time()
        res = solve_with_lp(attacker_actions, defender_actions, fp_cost_loss,
                            benign_data_prob)
        game_val, attacker_probs, probs_defender = res
        time_takens.append(time.time() - start)

    fp_part = calc_fals_positives(defender_actions, probs_defender,
                                  fp_cost_loss, benign_data_prob)

    final_experiment = Result(game_val,
                              fp_part,
                              game_val - fp_part,
                              -game_val,
                              attacker_actions, attacker_probs,
                              defender_actions, probs_defender,
                              l, time_takens)
    print("Experiment done.")

    # ----------------- Store final data -----------------
    experiment_data_file = f'{legacy_folder}/data'
    print(f'Storing final data to {experiment_data_file} file.')
    with open(experiment_data_file, 'wb') as file:
        pickle.dump(final_experiment, file)
    print('File saved')

    # ------- Do backup of configuration -----------------
    backup_object = {
        'features_num': features_num,
        'density': density,
        'latencies_available': l,
        'i_a': i_a,
        'i_d': i_d,
        'benign_ratio': benign_ratio,
        'data_file': data_file,
        'run_times': run_times,
        'legacy_folder': legacy_folder
    }
    backup_conf_file = f'{legacy_folder}/backup_conf'
    print(f'Storing backup conf to {backup_conf_file} file.')
    with open(backup_conf_file, 'wb') as file:
        pickle.dump(backup_object, file)
    print('File saved')

    print('Recapitulation:')
    print(f'Final value {game_val}')
    print(backup_conf_file)


if __name__ == "__main__":
    # Create base folder if it does not exist
    if not os.path.exists(legacy_folder):
        print(f'Creating base dir {legacy_folder}')
        os.mkdir(legacy_folder)

    log_file = f'{legacy_folder}/log'
    with open(log_file, 'a') as log:
        sys.stdout = log
        main()
    # main()
