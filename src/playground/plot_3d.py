import logging
from pathlib import Path

import torch
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

from data.loader import np_arrays_from_scored_csv
from neural_networks.network import FormattedData, NeuralNetwork
from playground.attacker_best_response import calc_optimal_br


def get_some_trained_nns(count: int) -> tuple:
    # Load data
    benign_x, _ = np_arrays_from_scored_csv(
        Path('test.csv'), 0, 500)
    malicious_x, _ = np_arrays_from_scored_csv(
        Path('scored_malicious.csv'), 1, 400)

    # Prepare benign data
    benign_unique_x, counts = np.unique(benign_x, axis=0, return_counts=True)
    probs_benign = np.array([count / len(benign_x) for count in counts])
    benign_y = np.zeros(len(benign_unique_x))
    benign_data = FormattedData(benign_unique_x, probs_benign, benign_y)

    # Prepare malicious data
    malicious_unique_x, counts = np.unique(malicious_x, axis=0,return_counts=True)
    probs_malicious = np.array([count / len(malicious_unique_x) for count in counts])
    malicious_y = np.ones(len(malicious_unique_x))
    malicious_data = FormattedData(malicious_unique_x, probs_malicious,malicious_y)

    # Create and train neural networks
    nns = []
    for i in range(count):
        nn = NeuralNetwork()
        nn.set_data(benign_data, malicious_data)
        nn.train()
        nns.append(nn)

    # Get random probability distribution
    probs = np.random.dirichlet(np.ones(count))
    return nns, probs


def f(x, y):
    return np.sin(np.sqrt(x ** 2 + y ** 2))


def setup_loger(debug: bool):
    log_format = ('%(asctime)-15s\t%(name)s:%(levelname)s\t'
                  '%(module)s:%(funcName)s:%(lineno)s\t%(message)s')
    level = logging.DEBUG if debug else logging.INFO
    logging.basicConfig(level=level, format=log_format)


if __name__ == "__main__":
    setup_loger(True)

    # Constants
    neural_network_count = 3
    points_count = 100

    # Train some neural networks
    nn, prob = get_some_trained_nns(neural_network_count)

    # Find optimal values with LP
    optimal_sol, optimal_gain = calc_optimal_br(nn, prob)

    # Calc and plot surface of attacker utility given trained neural networks
    x = np.linspace(0, 1, points_count)
    y = np.linspace(0, 1, points_count)
    X, Y = np.meshgrid(x, y)

    to_pred = []
    for ix in x:
        for iy in y:
            to_pred.append([iy, ix])

    total_gain = []
    for point_to_pred in to_pred:
        tmp_gain = 0
        for cur_nn, cur_prob in zip(nn, prob):
            pred = cur_nn.predict_single_limit(point_to_pred)
            tmp_gain += (1 - pred) * np.product(point_to_pred) * cur_prob

        total_gain.append(tmp_gain)
    total_gain = np.array(total_gain)
    Z = total_gain.reshape((points_count, points_count))

    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.set_xlabel('X feature')
    ax.set_ylabel('Y feature')
    ax.set_zlabel('attacker utility')
    ax.scatter(optimal_sol[0], optimal_sol[1], optimal_gain, c='r', marker='o', s=50)
    ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
                    cmap='viridis', edgecolor='none')
    ax.set_title('surface')
    plt.show()

