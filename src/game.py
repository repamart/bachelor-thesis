import logging

from src.config import RootConfig
from src.game_solver import GameSolver, Result

logger = logging.getLogger(__name__)


def setup_loger(debug: bool):
    log_format = ('%(asctime)-15s\t%(name)s:%(levelname)s\t'
                  '%(module)s:%(funcName)s:%(lineno)s\t%(message)s')
    level = logging.DEBUG if debug else logging.INFO
    logging.basicConfig(level=level, format=log_format)


class Game:
    def __init__(self, conf: RootConfig = RootConfig()):
        setup_loger(conf.debug)
        self._conf = conf
        self.result: Result = None

    def solve_game(self) -> Result:
        logger.info("Starting game solver")
        gs = GameSolver(self._conf)
        self.result = gs.double_oracle()

        self._write_summary()
        return self.result

    def _write_summary(self):
        print('\n\n-------------------------------------------------')
        logger.info(f'Game has ended with these values'
                    f'\ttransformed zero sum game value: {self.result.zero_sum_nash_val}'
                    f'\tattacker value of original game: {self.result.attacker_value}'
                    f'\tdefender value of original game: {self.result.defender_value}')

        logger.info('Attacker: action x probability')
        for a, p in zip(self.result.ordered_actions_p1, self.result.probs_p1):
            logger.info(f'{a} x {p}')

        print("\n")
        logger.info('Defender: action x probability')
        for nn, p in zip(self.result.ordered_actions_p2, self.result.probs_p2):
            logger.info(f'{nn} x {p}')
        print('-------------------------------------------------')


if __name__ == "__main__":
    Game().solve_game()
