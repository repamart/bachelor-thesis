from typing import List, Callable
from math import log
import attr
import pandas as pd


@attr.s
class Feature:
    name = attr.ib(factory=str)
    func = attr.ib(factory=Callable)  # Function f: str -> float


def remove_tld_from_domain(domain: str):
    return domain.rsplit('.', 1)[0]


class BigramCalculator:
    """
    Bigrams frequencies are calculated offline from 1 000 000 domains downloaded
    from majestic site

    majestic.com/reports/majestic-million
    """
    def __init__(self, file_name: str = 'bigrams_frequency3.csv'):
        dic = pd.read_csv(file_name).set_index('bigram').to_dict()
        self.bigram_dict = dic['frequency']

        # The most frequented bigram is 'in' with number of occurrences 154737
        # It means it's also the biggest score the domain might get
        self.max_frequency = 154737

    def bigram(self, domain: str):
        domain = remove_tld_from_domain(domain)
        bigrams_count = 0
        freq_sum = 0
        for subdomain in domain.split("."):
            for i in range(len(subdomain[:-1])):
                current = subdomain[i] + subdomain[i+1]
                freq_sum += self.bigram_dict.get(current, 0)
                bigrams_count += 1

        # Make a subtraction so the domains which have most in common with
        # natural language tends to zero and the synthetic strings to 1
        return self.max_frequency - (freq_sum / bigrams_count)

    def normalised_bigram(self, domain: str):
        return self.bigram(domain) / self.max_frequency


# Calculate entropy given the domain
def entropy(domain: str) -> float:
    word = remove_tld_from_domain(domain)
    e = 0.0
    length = len(word)
    occurrence = {}
    for letter in word:
        occurrence[letter] = occurrence.get(letter, 0) + 1
    for k, v in occurrence.items():
        p = v / length
        e -= p * log(p, 2)
    return e


# There is 63 allowed symbols from English alphabet to be used in domain, thus
# max entropy is equal to log_2(63) = 5.977
def norm_entropy(word: str) -> float:
    return entropy(word) / 5.977


# Calculate normalised domain length
def norm_len(word: str) -> float:
    return len(word) / 255


# Calculate occurrence of the unusual letters in a string
def uncommon_letters_score(domain: str) -> int:
    word = remove_tld_from_domain(domain)
    unusual = ('q', 'x', 'z', 'f')
    score = 0
    for letter in word.lower():
        if letter in unusual:
            score += 1
    return score


# Calculate domain normalised uncommon_letters_score
def normalised_letters_score(word: str) -> float:
    return uncommon_letters_score(word) / 255


# Create first line to .csv file with features
def initial_line(features: List[Feature], debug: bool = False) -> str:
    line = 'query' if debug else ''
    for feature in features:
        if line:
            line = f'{line},{feature.name}'
        else:
            line = feature.name
    return line


# Creates a line to .csv file with scored query based on given features
def score_query(features: List[Feature], query: str, debug: bool = False) -> str:
    line = query if debug else ''
    for feature in features:
        if line:
            line = f'{line},{feature.func(query)}'
        else:
            line = f'{feature.func(query)}'
    return line
