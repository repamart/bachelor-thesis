WHEELCACHE=wheelcache-bp:latest

cpu-image:
	docker build --build-arg IMAGE=python:3.7-slim --build-arg DEVICE=cpu --target wheelcache -t $(WHEELCACHE) .
	docker build --build-arg IMAGE=python:3.7-slim --build-arg DEVICE=cpu --target image -t experiments-martin-cpu:latest .

gpu-image:
	docker build --target wheelcache -t $(WHEELCACHE) .
	docker build --target image -t experiments-martin-gpu:latest .
