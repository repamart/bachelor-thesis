import os

import matplotlib.pyplot as plt
import numpy as np
import torch

from src.config import PlotterConfig
from src.data.loader import np_matrix_from_scored_csv

DEVICE = torch.device(os.environ.get('device', 'cuda'))


def plot_paths(self):
    for iteration, points in self.val_paths:
        plt.title("All iterations so far")
        plt.scatter([iteration], points[:1], c='blue', s=20)  # nash
        plt.scatter([iteration], points[1:2], c='red', s=10)  # attacker brp
        plt.scatter([iteration], points[2:3], c='green', s=10)  # defender brp
    plt.show()
    for iteration, points in self.val_paths[len(self.val_paths) - 5:]:
        plt.title("Last 5 iterations")
        plt.scatter([iteration], points[:1], c='blue', s=20)  # nash
        plt.scatter([iteration], points[1:2], c='red', s=10)  # attacker brp
        plt.scatter([iteration], points[2:3], c='green', s=10)  # defender brp
    plt.show()


class FakePlotter:
    """
    Fake plotter doing nothing used when num_features != 2
    """
    def plot_iteration(self, *args):
        pass


class LearningEpochsPlotter:
    def __init__(self, conf: PlotterConfig, discr_actions, data_filename):
        self.conf = conf

        if not self.conf.plot_enabled:
            return

        self.actions = torch.tensor(discr_actions).float().to(DEVICE)

        x, _ = np_matrix_from_scored_csv(data_filename, 0)
        self.dataset = np.unique(x, axis=0)

        # Variables to plot value of the game convergence
        self.last_zero_sum_value = None
        self.last_zero_sum_att_value = None
        self.last_zero_sum_def_value = None

        self.fig, self.ax = plt.subplots()
        self.ax.set_xlabel('Double oracle iteration')
        self.ax.set_ylabel('Zero-sum game value')

    def plot_iteration(self, iteration, zero_sum_val, p1_br_value, p2_br_value,
                        played_p2, probs_p2, played_p1, probs_p1, br_p1, br_p2):
        if not self.conf.plot_enabled:
            return

        # fig, ax = plt.subplots()
        # plt.tight_layout()
        # ax.set_xlim(0., 1.)
        # ax.set_ylim(0., 1.)

        # # ----------- Plot defender best response -----------------------
        # # Plot benign data-set --- TMP ---
        # ax.scatter(self.dataset[:, 0], self.dataset[:, 1], c='blue', s=1.)
        # # ---------------
        #
        # # Plot defender's best response
        # res = br_p2.latency_predict(self.actions) \
        #     .cpu().numpy().reshape((101, 101), order='F')
        # ax.imshow(res, cmap='Reds', vmin=0, vmax=1, origin='lower', extent=(0., 1., 0., 1.))
        #
        # # Plot attacker nash strategy
        # for point, prob in zip(played_p1, probs_p1):
        #     if prob == 0:
        #         continue
        #     ax.scatter(point[0], point[1], c='green', marker='^')
        #     ax.annotate(f'{round(prob, 2)}', (point[0], point[1]), color='green')

        # -----------Plot game value convergence----------------------
        if self.last_zero_sum_value is not None:
            x_vals = [iteration - 1, iteration]
            self.ax.plot(x_vals, [self.last_zero_sum_value, zero_sum_val], c='blue')
            self.ax.plot(x_vals, [self.last_zero_sum_att_value, p1_br_value], c='red')
            self.ax.plot(x_vals, [self.last_zero_sum_def_value, p2_br_value], c='green')
        self.last_zero_sum_value = zero_sum_val
        self.last_zero_sum_att_value = p1_br_value
        self.last_zero_sum_def_value = p2_br_value

        self.fig.savefig(f'{self.conf.output_svg_dir}/convergence_iteration{iteration}.svg',
                    format='svg', bbox_inches='tight')


class DebugPlotter:
    def __init__(self, conf: PlotterConfig, discr_actions, data_filename):
        self.conf = conf

        if not self.conf.plot_enabled:
            return

        self.plotted = []
        self._init_plots(discr_actions, data_filename)

        # Variables to plot value of the game convergence
        self.last_zero_sum_value = None
        self.last_zero_sum_att_value = None
        self.last_zero_sum_def_value = None

    def _init_plots(self, discr_actions, data_filename):
        plt.ion()
        self.fig, self.ax = plt.subplots(2, 3)
        self.actions = torch.tensor(discr_actions).float().to(DEVICE)
        self.plotted = []

        # TMP-----------
        x, _ = np_matrix_from_scored_csv(data_filename, 0)
        x = np.unique(x, axis=0) * 100
        self.ax[0][0].scatter(x[:, 0], x[:, 1], c='blue', s=1.)
        # ---------------

        self.ax[0][0].set_title('Defender nash strategy')

        self.ax[0][1].set_title('Attacker nash strategy')
        self.ax[0][1].set_xlim([0, 1])
        self.ax[0][1].set_ylim([0, 1])

        self.ax[0][2].set_title('All attackers actions played')
        self.ax[0][2].set_xlim([0, 1])
        self.ax[0][2].set_ylim([0, 1])

        self.ax[1][0].set_title('Defender best response')

        self.ax[1][1].set_title('Attacker best response')
        self.ax[1][1].set_xlim([0, 1])
        self.ax[1][1].set_ylim([0, 1])

        self.ax[1][2].set_title('Convergence of value of the game')
        plt.tight_layout()

    def plot_iteration(self, *args):
        if not self.conf.plot_enabled:
            return

        # Remove all lines from previous iteration plotting
        for item in self.plotted:
            item.remove()
        self.plotted = []

        # Do the actual plotting
        self._plot_iteration(*args)

        # Show the result
        self.fig.canvas.draw()
        plt.pause(0.000001)

    def _plot_iteration(self, iteration, zero_sum_val, p1_br_value, p2_br_value,
                        played_p2, probs_p2, played_p1, probs_p1, br_p1, br_p2):
        # Set title of current figure
        self.fig.suptitle(f'Iteration: {iteration}, value: {zero_sum_val}')

        # Plot heat-map of defender's nash strategy actions
        res = np.zeros((101, 101))
        for nn, prob in zip(played_p2, probs_p2):
            if prob == 0: continue
            predictions = nn.latency_predict(self.actions).cpu().numpy()
            res += (predictions * prob).reshape((101, 101), order='F')
        self.plotted.append(self.ax[0][0].imshow(res, cmap='Reds', vmin=0,
                                                 vmax=1, origin='lower'))

        # Plot attacker nash strategy
        for point, prob in zip(played_p1, probs_p1):
            if prob == 0:
                continue
            self.plotted.append(self.ax[0][1].scatter(point[0], point[1],
                                                      c='red', marker='^'))
            self.plotted.append(self.ax[0][1].annotate(f'{round(prob, 2)}',
                                                       (point[0], point[1])))

        # Add attacker new action to subplot with all his actions
        self.ax[0][2].scatter(br_p1[0], br_p1[1], c='blue', marker='^')

        # Plot heat-map of defender's best response
        res = br_p2.latency_predict(self.actions).cpu().numpy().reshape((101, 101), order='F')
        self.plotted.append(self.ax[1][0].imshow(res, cmap='Reds', vmin=0,
                                                 vmax=1, origin='lower'))
        # Plot attacker best response
        self.plotted.append(self.ax[1][1].scatter(br_p1[0], br_p1[1], c='red'))

        # Plot game value convergence
        if self.last_zero_sum_value is not None:
            x_vals = [iteration - 1, iteration]
            self.ax[1][2].plot(x_vals, [self.last_zero_sum_value, zero_sum_val],
                               c='blue')
            self.ax[1][2].plot(x_vals, [self.last_zero_sum_att_value, p1_br_value],
                               c='red')
            self.ax[1][2].plot(x_vals, [self.last_zero_sum_def_value, p2_br_value],
                               c='green')

        self.last_zero_sum_value = zero_sum_val
        self.last_zero_sum_att_value = p1_br_value
        self.last_zero_sum_def_value = p2_br_value
