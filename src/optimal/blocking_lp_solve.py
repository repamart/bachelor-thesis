import functools
import itertools
import operator
import sys
from collections import Counter
from typing import List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pulp

from data.loader import np_matrix_from_scored_csv

# Global "config" variables
features_num = 2
density = 2  # 1 is low | 2 is high
FP_cost = 1

benign_data_file = 'test.csv'  # TODO use proper data


def create_actions() -> List:
    if density == 1:
        one_axis = np.linspace(0, 1, 11)  # [0, 0.1, 0.2, ..., 0.9, 1]
    elif density == 2:
        one_axis = np.linspace(0, 1, 101)  # [0, 0.01, 0.02, ..., 0.99, 1]
    else:
        print(f'Density is not high (2) neither low (1). It is {density}')
        sys.exit(5)

    one_axis = np.round(one_axis, density)  # To get rid of bad double precision
    generator = itertools.product(*itertools.repeat(one_axis, features_num))
    return list(generator)


def get_data() -> dict:
    benign, _ = np_matrix_from_scored_csv(benign_data_file, 0)

    # Round to proper density
    benign = list(
        map(lambda x: tuple(map(lambda y: round(y, density), x)), benign))

    benign_data_prob = Counter(benign)
    for key, val in benign_data_prob.items():
        benign_data_prob[key] = val / len(benign)
    return benign_data_prob


def utility(attacker_action: Tuple):
    return functools.reduce(operator.mul, attacker_action, 1)


def solve_with_lp(actions_attacker: List,
                  actions_defender: List,
                  benign_data_prob: dict):
    print('Going to solve with LP')
    print(f'Attacker\'s actions: {actions_attacker}')
    print(f'Defender\'s actions: {actions_defender}')
    print(f'Benign data probabilities are: {benign_data_prob}')

    # Create LP problem
    m = pulp.LpProblem("Zero sum game", pulp.LpMinimize)

    # Minimizing value "v"
    v = pulp.LpVariable("v")
    m += v

    # Player two probabilities vector
    print("Defining defenders probabilities...")
    probs_defender = []
    for action in defender_actions:
        probs_point = pulp.LpVariable(f'p({action[0]},{action[1]})', 0, 1)
        probs_defender.append(probs_point)

    fp_cost = 0
    num_of_fp = 0
    for action, prob in zip(defender_actions, probs_defender):
        cur_fp = prob * benign_data_prob[action]
        num_of_fp += cur_fp
        fp_cost += cur_fp * FP_cost

    print("Defining main constraint...")
    constraints = []
    for i in range(len(attacker_actions)):
        sum = [fp_cost, (1 - probs_defender[i]) * utility(attacker_actions[i])]
        constraints.append(pulp.lpSum(sum) <= v)
    for c in constraints:
        m += c

    print("Ok, let's solve now...")
    m.solve()

    print(f'LP solved')
    print(f'Value of the zero sum game: {v.varValue}')
    print(f'Found solution: {pulp.LpStatus[m.status]}')
    print(f'Attacker\'s probabilities:')
    for i in range(len(constraints)):
        prob = abs(constraints[i].pi)
        if prob == 0: continue
        print(f'Action {attacker_actions[i]} -> {prob}')

    print(f'Deffender\'s probabilities to block:')
    for i in range(len(probs_defender)):
        print(f'Action {attacker_actions[i]} -> {probs_defender[i].varValue}')

    # Return value and probabilities
    return num_of_fp.value(), v.varValue, [abs(c.pi) for c in constraints], \
           [prob.varValue for prob in probs_defender]


def plot_summarization(def_actions, def_probs, at_actions, at_probs, value):
    plt.ion()
    fig, ax = plt.subplots(1, 1)

    ax.set_title(f'Optimal value from LP | only blocking')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_xlim(-0.1, 1.1)
    ax.set_ylim(-0.1, 1.1)

    # Plot defender
    for point, prob in zip(def_actions, def_probs):
        if prob is None:  # Strange hack
            prob = 0

        ax.scatter(point[0], point[1], c=[pick_color(prob)])
        if prob != 0 and prob != 1:
            ax.annotate(f'n={round(prob, 3)}', (point[0], point[1]),
                        weight="bold")

    # Plot attacker
    for point, prob in zip(at_actions, at_probs):
        if prob == 0:
            continue
        ax.scatter(point[0], point[1] - 0.01, c='blue', marker='x')
        ax.annotate(f'{round(prob, 3)}', (point[0], point[1] - 0.03),
                    weight='bold', color='blue')

    # Hack the legend
    ax.plot([], [], ' ', label=f'Final value of the game = {round(value, 3)}')
    ax.scatter([100], [100], c='red', label='block with prob 100%')
    ax.scatter([100], [100], c='green', label='block with prob 0%')
    ax.scatter([100], [100], c='blue', marker='x', label='Attacker actions')
    ax.scatter([100], [100], c='purple', label='block with n %')
    ax.legend(prop={'size': 12})


def pick_color(prob):
    if prob == 1:
        return 1, 0, 0  # red
    elif prob == 0:
        return 0, 1, 0  # green
    else:
        return 0.5, 0, 0.5  # purple


if __name__ == "__main__":
    # Get actions
    attacker_actions = create_actions()
    defender_actions = create_actions()

    # load data, round data and make occurrences dict
    benign_probs = get_data()

    # Solve
    res = solve_with_lp(attacker_actions, defender_actions, benign_probs)
    fp_sum, game_val, attacker_probs, probs_defender = res

    # Plot results
    plot_summarization(defender_actions, probs_defender,
                       attacker_actions, attacker_probs, game_val)
    print("DONE")
