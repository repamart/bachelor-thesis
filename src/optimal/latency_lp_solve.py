import functools
import itertools
import operator
from collections import Counter
from typing import List, Tuple, Callable

import matplotlib.pyplot as plt
import numpy as np
import pulp
from os.path import dirname

from data.loader import np_matrix_from_scored_csv


def solve_with_lp(actions_attacker: List,
                  actions_defender: List,
                  u: Callable,
                  loss: Callable,
                  benign_data_prob: dict,
                  l: List):
    print('Going to solve with LP')
    print(f'Attacker\'s actions by now: {actions_attacker}')
    print(f'Defender\'s actions by now: {actions_defender}')
    print(f'Benign data probabilities are: {benign_data_prob}')

    # Create LP problem
    m = pulp.LpProblem("Zero sum game", pulp.LpMinimize)

    # Minimizing value "v"
    v = pulp.LpVariable("v")
    m += v

    # Player two probabilities vector
    print("Defining defenders probabilities...")
    probs_defender = []
    for action in defender_actions:
        probs_point = [pulp.LpVariable(f'p({action[0]},'
                                       f'{action[1]},'
                                       f'{k})', 0, 1) for k in l]
        m += pulp.lpSum(probs_point) == 1  # Probabilities sum to 1
        probs_defender.append(probs_point)

    fp_cost = 0
    for action, prob in zip(defender_actions, probs_defender):
        for j in range(len(l)):
            fp_cost += loss(l[j])*benign_data_prob[tuple(action)]*prob[j]

    print("Defining main constraint...")
    constraints = []
    for i in range(len(attacker_actions)):
        print(f'Defining constraint for {i}')
        suma = [fp_cost]
        for j in range(len(l)):
            suma.append(probs_defender[i][j] * u(attacker_actions[i], l[j]))
        constraints.append(pulp.lpSum(suma) <= v)
    for c in constraints:
        m += c

    print("Ok, let's solve now...")
    m.solve()

    print(f'LP solved')
    print(f'Value of the game: {v.varValue}')
    print(f'Found solution: {pulp.LpStatus[m.status]}')
    print(f'Attacker\'s probabilities:')
    print(f'{list(str(abs(c.pi)) + " " for c in constraints)}')
    print(f'Deffender\'s probabilities:')
    for probs in probs_defender:
        print(f'{list(str(item.varValue) + " " for item in probs)}')

    return v.varValue, [abs(c.pi) for c in constraints], [prob for prob in
                                                          probs_defender]


def create_attacker_actions():
    # one_axis = np.linspace(0, 1, 11)  # [0, 0.1, 0.2, ..., 0.9, 1]
    one_axis = np.linspace(0, 1, 101)  # [0, 0.1, 0.2, ..., 0.9, 1]
    return list(itertools.product(one_axis, one_axis))


def create_defenders_actions():
    # one_axis = np.linspace(0, 1, 11)  # [0, 0.1, 0.2, ..., 0.9, 1]
    one_axis = np.linspace(0, 1, 101)  # [0, 0.1, 0.2, ..., 0.9, 1]
    return list(itertools.product(one_axis, one_axis))


def utility(attacker_action: Tuple, l):
    return functools.reduce(operator.mul, attacker_action, 1) * (1 - l)


def plot_summarization(def_actions, def_probs, l, at_actions, at_probs, value):
    fig = plt.gcf()
    fig.set_size_inches(19, 11, forward=True)
    plt.title(f'Optimal solution with rate limiting option in utility '
              f'(not as lp constraint)')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.xlim(-0.1, 1.1)
    plt.ylim(-0.1, 1.1)
    # Plot defender
    for point, def_probs in zip(def_actions, def_probs):
        notation = ''
        for i in range(len(l)):
            # No need to show no probability
            if def_probs[i].varValue == 0:
                continue

            # There is color in case of doing nothing or blockin 100%
            if def_probs[i].varValue == 1 and (l[i] == 1 or l[i] == 0):
                notation = ''
                break
            addition = f'{l[i]}->{round(def_probs[i].varValue, 2)}'
            notation = f'{notation}\n{addition}' if notation else f'{addition}'

        plt.scatter(point[0], point[1], c=[pick_color(def_probs, l)])
        plt.annotate(notation, (point[0], point[1]), weight="bold")

    # Plot attacker
    for point, prob in zip(at_actions, at_probs):
        if prob == 0:
            continue
        plt.scatter(point[0], point[1] - 0.01, c='blue', marker='x')
        plt.annotate(f'{round(prob, 2)}', (point[0], point[1] - 0.03),
                     weight='bold', color='blue')


    # Plot vrstevnice, jen pro test
    for i in range(10, -10, -1):
        x = np.linspace(0, 1, 100)
        plt.plot(x, (i/10)/x, alpha=0.1)


    # Hack the legend
    plt.plot([], [], ' ', label=f'Final value of the game = {round(value, 2)}')
    plt.plot([100], [100], c='blue', label='\"FP\" loss function')
    plt.scatter([100], [100], c='red', label='block 100%')
    plt.scatter([100], [100], c='green', label='Do nothing')
    plt.scatter([100], [100], c='blue', marker='x', label='Attacker actions')
    plt.scatter([100], [100], s=0, label='limit -> prob')
    plt.legend(prop={'size': 12})

    # Show/save
    plt.savefig(f'{dirname(__file__)}/../../results/optimal/'
                f'{CURRENT_RUN}-summarization.png')
    # plt.show()
    plt.close(fig)


def pick_color(probs, l):
    most_prob = 0
    val = 0
    for prob, limit in zip(probs, l):
        if prob.varValue > most_prob:
            most_prob = prob.varValue
            val = limit

    if val > 0.5:
        r = 1
        g = 2 * (1 - val)
    else:
        g = 1
        r = val * 2
    b = 0
    return r, g, b


def plot_loss(loss):
    # Plot first plot only with loss function
    fig, ax = plt.subplots()
    ax.set_title(f'Cost for limiting benign dataset')
    ax.set_xlabel('limit')
    ax.set_ylabel('cost')
    ax.set_xlim(0, 1)
    ax.set_ylim(0, loss(1))
    x = np.linspace(0, 1, 100)
    ax.plot(x, loss(x))
    fig.savefig(f'{dirname(__file__)}/../../results/optimal/'
                f'{CURRENT_RUN}-loss.png')
    plt.close(fig)

    # Start another plot, later adding a defender probabilities
    plt.xlim(0, 1)
    plt.ylim(0, 1)

    x = np.linspace(0, 1, 100)
    plt.plot(x, loss(x), alpha=0.5)

CURRENT_RUN = 5  # To name figures files in each iteration
if __name__ == "__main__":
    l = [.0, .25, .50, .75, 1.0]
    # l = [.0, .1, .2, .3, .4, .5, .6, .7, .8, .9, 1.]

    attacker_actions = create_attacker_actions()
    attacker_actions = list(
        map(lambda x: list(map(lambda y: round(y, 2), x)), attacker_actions))

    defender_actions = create_defenders_actions()
    defender_actions = list(
        map(lambda x: list(map(lambda y: round(y, 2), x)), defender_actions))

    u = utility

    # load data, round data and make occurrences dict
    benign = np_matrix_from_scored_csv('test.csv', 0)[0]
    benign_data = list(
        map(lambda x: tuple(map(lambda y: round(y, 2), x)), benign))
    benign_data_prob = Counter(benign_data)
    for key, val in benign_data_prob.items():
        benign_data_prob[key] = val/len(benign_data)

    # Define testing loss functions
    test_loss_cost_values = [lambda x: x ** 2]
                             # lambda x: x,
                             # lambda x: 5 * x ** 2,
                             # lambda x: 5 * x ** 3,
                             # lambda x: 3 * x ** 3,
                             # lambda x: 5 * x ** 4,
                             # lambda x: 5 * x ** 10]


    # Run test for all values
    for loss_test in test_loss_cost_values:
        res = solve_with_lp(attacker_actions, defender_actions, u,
                            loss_test, benign_data_prob, l)
        game_val, attacker_probs, probs_defender = res
        break
        # Plot results
        plot_loss(loss_test)
        plot_summarization(defender_actions, probs_defender, l,
                           attacker_actions, attacker_probs, game_val)
        CURRENT_RUN += 1

    print("DONE")
