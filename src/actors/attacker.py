import itertools
import logging
import operator
import os
from typing import List

import numpy as np
import torch

from src.config import ModelConfig

logger = logging.getLogger(__name__)
DEVICE = torch.device(os.environ.get('device', 'cuda'))


class Attacker:
    def __init__(self, model_conf: ModelConfig):
        self.conf = model_conf.attacker_conf
        self.features_count = model_conf.features_count

    def get_conf(self):
        return self.conf

    def random_action(self) -> List:
        # Return random set of features as action
        return [np.random.uniform(0.0, 1.0) for _ in range(self.features_count)]

    def get_initial_action(self) -> List:
        # return [1., 1.]
        # return [0.59897846, 0.2900984]
        return self.random_action()

    def get_best_response(self, def_actions: List, def_probs: List):
        logger.info('Attacker searching for response')
        # Take only defenders actions which are played with non zero probability
        non_zero_p = np.where(np.asarray(def_probs) != 0)
        actions = np.asarray(def_actions)[non_zero_p]
        probs = np.asarray(def_probs)[non_zero_p]

        br = self._get_best_response(actions, probs)
        logger.info(f'Attacker found BR {br}')
        return br

    def does_br_exists(self, played_actions_p1, br_p1, value):
        it_does = self._does_br_exists(played_actions_p1, br_p1, value)
        if it_does:
            logger.debug('This attacker action already exists')
        else:
            logger.debug('This attacker action does not exist yet')
        return it_does

    def create_discrete_actions(self):
        one_axis = np.linspace(0, 1, 101)  # [0.00, 0.01, 0.02, ..., 0.99, 1.00]
        repeat = self.features_count - 1
        generator = itertools.product(one_axis, *itertools.repeat(one_axis, repeat))
        return np.array(list(generator))

    def _get_best_response(self, def_actions: List, def_probs: List) -> List:
        raise NotImplementedError()

    def _does_br_exists(self, played_actions_p1, br_p1, value):
        raise NotImplementedError()


class DiscreteAttacker(Attacker):
    def __init__(self, model_conf: ModelConfig):
        super().__init__(model_conf)
        self.utility = model_conf.attacker_utility
        self.actions = super().create_discrete_actions()

    def _get_best_response(self, def_actions: List, def_probs: List) -> List:
        best_rp = max(self.actions, key=lambda a1: sum(map(operator.mul, map(
            lambda a2: self.utility(a1, a2), def_actions), def_probs)))

        # Calculate optimal value
        self.value_of_last_brp = sum(map(operator.mul, map(lambda nn:
                            self.utility(best_rp, nn), def_actions), def_probs))
        return list(best_rp)

    def _does_br_exists(self, played_actions_p1, br_p1, value):
        return self.value_of_last_brp - value < self.conf.epsion


class GradientAttacker(Attacker):
    def __init__(self, model_conf: ModelConfig):
        super().__init__(model_conf)
        self.torch_utility = model_conf.attacker_torch_utility

    def _get_best_response(self, def_actions: List, def_probs: List):
        all_actions = []
        for _ in range(self.conf.tries_for_best_response):
            all_actions.append(super().random_action())
        all_actions = torch.tensor(all_actions, requires_grad=True, device=DEVICE)
        optimizer = torch.optim.Adam([all_actions], lr=self.conf.learning_rate)

        for _ in range(self.conf.epochs):
            losses = 0
            for nn, prob in zip(def_actions, def_probs):
                losses += -(self.torch_utility(all_actions, nn) * prob)
            loss = torch.mean(losses)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # Clamp input tensor to allowed range after gradient descent update
            all_actions.data.clamp_(min=0.0, max=1.0)

        i = torch.argmin(losses)
        best_action = all_actions[i]
        self.value_of_last_brp = -losses[i].item()
        return [best_action[i].item() for i in range(self.features_count)]

    def _does_br_exists(self, played_actions_p1, br_p1, value):
        return self.value_of_last_brp - value < self.conf.epsion
