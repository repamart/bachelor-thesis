# bachelor-thesis

Repository with reference implementation of game theoretical models related to
my bachelor-thesis (and software/research project). Source code is written in python.

### Prerequisites

* Python3.6 or higher _(for static typing)_
* Pipenv - package managing tool (get with `pip install --user pipenv`)

### Run 

For running take the following steps after cloning the project:

Firstly to download dependencies run from the project root directory

`pipenv sync`

Then to discover project repositories run setup.py

`pipenv run python pip install -e .`

And finally run the app from the root directory

`pipenv run python src/main.py`

To modify default configuration of the project see src/[config.py](src/config.py) file

## Documentation

* Documentation can be found [here](report). Doc is currently in progress.
