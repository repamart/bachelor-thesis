import itertools
import logging
import operator
import time
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import torch

from config import RootConfig
from data.loader import np_arrays_from_scored_csv
from neural_networks.network import FormattedData, NeuralNetwork

logger = logging.getLogger(__name__)
optimal_best_response = None


def setup_logger(debug: bool):
    log_format = ('%(asctime)-15s\t%(name)s:%(levelname)s\t'
                  '%(module)s:%(funcName)s:%(lineno)s\t%(message)s')
    level = logging.DEBUG if debug else logging.INFO
    logging.basicConfig(level=level, format=log_format)


def plot_gradient_path(points: [], colors: ()):
    plt.title(f'Attacker gradient descent')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.xlim(0, 1)
    plt.ylim(0, 1)

    for point, c in zip(points, colors):
        plt.scatter(point[0], point[1], c=[c], s=0.7)

    plt.scatter(optimal_best_response[0], optimal_best_response[1], c='red')
    plt.annotate('Optimal by LP', optimal_best_response, color='red')

    plt.show()


def get_some_trained_nns(count: int) -> tuple:
    # Load data
    benign_x, _ = np_arrays_from_scored_csv(
        Path('all_benign_scored.csv'), 0, 500)
    malicious_x, _ = np_arrays_from_scored_csv(
        Path('scored_malicious.csv'), 1, 400)

    # Prepare benign data
    benign_unique_x, counts = np.unique(benign_x, axis=0, return_counts=True)
    probs_benign = np.array([count / len(benign_x) for count in counts])
    benign_y = np.zeros(len(benign_unique_x))
    benign_data = FormattedData(benign_unique_x, probs_benign, benign_y)

    # Prepare random malicious data for each neural network
    malicious_data = []
    for _ in range(count):
        malicious_x = np.array([[np.random.uniform(0, 1), np.random.uniform(0, 1)] for _ in range(10)])
        probs_malicious = np.random.dirichlet(np.ones(len(malicious_x)))
        malicious_y = np.ones(len(malicious_x))
        malicious_data.append(FormattedData(malicious_x, probs_malicious, malicious_y))

    # Create and train neural networks
    nns = []
    for i in range(count):
        conf = RootConfig()
        nn = NeuralNetwork(2, conf.model_conf.defender_conf.nn_conf)
        nn.set_data(benign_data, malicious_data[i])
        nn.train()
        nns.append(nn)

    # Get random probability distribution
    probs = np.random.dirichlet(np.ones(count))
    return nns, probs


def find_attacker_best_responses(nns: List, probs: List, count: int, plot_path=True):
    learning_rate = 0.5e-2

    # colors = [tuple(np.random.uniform(0,1) for _ in range(3)) for _ in range(count)]
    attacker_actions = torch.tensor([[np.random.uniform(0, 1), np.random.uniform(0, 1)] for _ in range(count)], requires_grad=True)
    optimizer = torch.optim.Adam([attacker_actions], lr=learning_rate)

    for i in range(200):
        losses = 0
        for nn, prob in zip(nns, probs):
            prediction = nn.latency_predict(attacker_actions, with_grad=True)
            tmp_loss = -(torch.add(1, -prediction) * torch.prod(
                attacker_actions, dim=1) * prob)
            losses += tmp_loss
        loss = torch.mean(losses)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        logger.debug("".join(f'{round(a.item(),4)}\t' for a in tmp_loss))

        # Clamp input tensor to allowed range after gradient descent update
        attacker_actions.data.clamp_(min=0.0, max=1.0)

    i = torch.argmin(losses)
    best_response = attacker_actions[i]
    best_value = -losses[i].item()
    return tuple([best_response[0].item(), best_response[1].item()]), best_value


def calc_optimal_br(nns: List, probs: List) -> tuple:
    one_axis = np.linspace(0, 1, 101)
    attacker_actions = list(itertools.product(one_axis, *itertools.repeat(one_axis, 1)))
    optimal_solution = max(attacker_actions, key=lambda a: sum(map(operator.mul,
                       map(lambda nn: (1 - nn.predict_single_latency(a)) *
                                       a[0] * a[1], nns), probs)))
    optimal_gain = sum(map(operator.mul,
                       map(lambda nn: (1 - nn.predict_single_latency(optimal_solution)) *
                                       np.product(optimal_solution), nns), probs))
    return optimal_solution, optimal_gain


if __name__ == '__main__':
    setup_logger(True)

    # Get some random defender's neural network
    nns, probs = get_some_trained_nns(5)

    # Find optimal best response using LP
    optimal_best_response, optimal_value = calc_optimal_br(nns, probs)

    # attacker's best responses
    attacker_brs_gradient_descent, grad_value = find_attacker_best_responses(nns, probs, 264)

    print(f'Optimal by LP: action: {optimal_best_response} | loss: {optimal_value}')
    print(f'Gradient Desc: action: {attacker_brs_gradient_descent} | loss: {grad_value}')
    print(f'Gradient was better by: {grad_value - optimal_value}')
